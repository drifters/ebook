# @drifters/ebook

> Curl builder used to dynamically building curls

[![Generic badge](https://img.shields.io/badge/Bitbucket-@drifters/curl--builder-green?logo=bitbucket&style=flat&labelColor=0052CC&color=6C2E84&link=http://left&link=http://right)](https://bitbucket.org/bdrifter/drifters-library/src/master/packages/utils/ebook/)
[![CircleCI](https://circleci.com/bb/drifters/ebook.svg?style=shield)](https://circleci.com/bb/drifters/ebook)
[![codecov](https://codecov.io/bb/drifters/ebook/branch/master/graph/badge.svg)](https://codecov.io/bb/drifters/ebook)
[![Babel](https://img.shields.io/badge/Babel-7.9.0-green?logo=Babel&style=flat&color=F9DC3E&link=http://left&link=http://right)](https://bitbucket.org/bdrifter/drifters-library/src/master/)
[![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest)

## Install

```bash
npm i --save @drifters/ebook
```
## Example

```
const { ebook, log } = require('@drifters/ebook');

ebook.info('a')
log('a')
```
## License

MIT © [suminksudhi](https://github.com/suminksudhi)
