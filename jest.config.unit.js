module.exports = {
  testEnvironment: 'node',
  moduleDirectories: ['<rootDir>/node_modules/'],
  testPathIgnorePatterns: [],
  collectCoverage: true,
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  coverageReporters: ["json", "lcov", "text", "clover", "text-summary"],
  coveragePathIgnorePatterns: [],
  collectCoverageFrom: ['<rootDir>/src/**/*.js'],
  coverageDirectory: 'coverage',
  setupFilesAfterEnv: ['<rootDir>/jest/reporterConfig.js'],
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
};
