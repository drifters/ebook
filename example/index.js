const axios = require('axios');
import { convertToPdf, genEpub } from '../src';


/**
 * @deprecated
 * @param url
 * @returns {Promise<* | void>}
 */
export const genEbook = async ({ url }) => axios.get('http://www.gutenberg.org/files/2701/2701-0.txt')
    .then(res => res.data)
    .then(text => {
        console.log(text);
        text = text.slice(text.indexOf('EXTRACTS.'));
        text = text.slice(text.indexOf('CHAPTER 1.'));

        const lines = text.split('\r\n');
        const content = [];
        for (let i = 0; i < lines.length; ++i) {
            const line = lines[i];
            if (line.startsWith('CHAPTER ')) {
                if (content.length) {
                    content[content.length - 1].data = content[content.length - 1].data.join('\n');
                }
                content.push({
                    title: line,
                    data: ['<h2>' + line + '</h2>']
                });
            } else if (line.trim() === '') {
                if (content[content.length - 1].data.length > 1) {
                    content[content.length - 1].data.push('</p>');
                }
                content[content.length - 1].data.push('<p>');
            } else {
                content[content.length - 1].data.push(line);
            }
        }
        return genEpub({
            title: 'Moby-Dick',
            author: 'Herman Melville',
            filePath: './moby-dick.epub',
            content
        });
    })
    .then(() => {
        console.log('Done for epub now pdf ')
        convertToPdf({})
    })
    .catch((e) => console.log(e));

genEbook().then(() => console.log(done))