const epub = require('epub-gen');
const path = require('path');
const { spawn } = require('child_process');
const { lookpath } = require('lookpath');

/**
 * execute a given command
 * @param command
 * @param options
 * @param enableLog
 */
const executeCommand =
    /* istanbul ignore next */
    async ({ command, args, options, input, enableLog = false }) => {
    return new Promise((resolve, reject) => {
        const subprocess = spawn(command, args, options);
        let output = '';
        subprocess.stdout.on('data', (data) => {
            enableLog && console.log(`${data}`);
            output += data;
        });

        subprocess.stderr.on('data', (data) => {
            enableLog && console.error(`${chalk.yellow(data)}`);
            output += data;
        });

        subprocess.on('error', (err) => {
            enableLog && console.error('Failed to start subprocess.');
            reject(err.toString())
        });

        subprocess.on('close', (code) => {
            if(code !== 0) {
                enableLog && console.log(`child process exited with code ${code}`);
                return reject(1)
            }
            resolve(code)
        });

        if(input) {
            subprocess.stdin.write(input);
            subprocess.stdin.end();
        }

        return subprocess;
    })
};


/**
 * used to convert a epub to a pdf
 * @returns {Promise<void>}
 */
export const convertToPdf = async ({
    input = path.join(process.cwd(), './moby-dick.epub'), //path to epub
    output = path.join(process.cwd(), './moby-dick.pdf'), //path to pdf
}) => {
    const p = await lookpath('ebook-convert');
    if (p) {
        await executeCommand({command: 'ebook-convert', args: [input, output], enableLog: false})
    }
}

/**
 * used to generate the epub
 * @param title
 * @param author
 * @param filePath
 * @param content
 * @returns {Promise<*>}
 */
export const genEpub = async ({ title, author = 'Drifters', filePath, content }) => new epub({
        title,
        author,
        output: filePath,
        content
    }).promise;