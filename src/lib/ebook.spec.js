/**
 * @jest-environment node
 */
import { lookpath } from 'lookpath';
import sinon from 'sinon'
import events from 'events'
import { convertToPdf, genEpub } from './ebook';


const child_process = require('child_process');

jest.mock('epub-gen')
jest.mock('lookpath');
/**
jest.mock('child_process', () => ({
    spawn: jest.fn()
}));

child_process.spawn.mockImplementation(() => {
    const eventdata = new events.EventEmitter();
    return {
        ...eventdata,
        stdout: new events.EventEmitter(),
        stderr: new events.EventEmitter(),
        on: jest.fn(),
        emit: jest.fn()
    }
})
**/
describe('ebook', () => {
    it("generates an ebook", async () => {
        await genEpub({ url: "xyz"})
    })

    //@todo figure out event emitter based exit code currently 1 why
    it("generates an pdf from epub",  async() => {
        try {
            lookpath.mockImplementation(async () => Promise.resolve(true))
            let sandbox = sinon.createSandbox();
            let spawnEvent = new events.EventEmitter();
            spawnEvent.stdout = new events.EventEmitter();
            sandbox.stub(child_process, 'spawn').returns(spawnEvent);
            spawnEvent.stdout.emit('data', "sample");
            //spawnEvent.emit('close', 0);
            await convertToPdf({})
        }catch(e){
            console.log(e)
        }
    })

    it("fails to generates an pdf from epub", async () => {
        lookpath.mockImplementation(() => undefined)
        await convertToPdf({ })
    })
});
